<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\City;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $client = Client::with('city')->paginate(10);

        return view('client.index', ['client' => $client]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('client.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'cod' => 'required'
        ]);

        $allInputs = $request->all();

        $client = new Client();
        $client->cod = $allInputs['cod'];
        $client->name = $allInputs['name'];
        $client->save();

        $city = new City();
        $city->cod = $allInputs['cod'];
        $city->name = $allInputs['city'];
        $client->city()->save($city);

        return redirect()->route('client.index')
            ->with('success', 'Client created successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        return view('client.edit', compact('client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        $request->validate([
            'name' => 'required',
            'cod' => 'required'
        ]);

        $allInputs = $request->all();

        $client = Client::find($allInputs['id']);
        $client->cod = $allInputs['cod'];
        $client->name = $allInputs['name'];
        $client->city->name = $allInputs['city'];
        $client->push();

        return redirect()->route('client.index')
            ->with('success', 'Client created successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        $client->delete();

        return redirect()->route('client.index')
            ->with('success', 'Client deleted successfully');
    }
}
