<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Client;

class City extends Model
{
    use HasFactory;

    protected $table = 'cities';

    protected $fillable = [
        'user_id',
        'cod',
        'name'
    ];

    public function client() {
        return $this->belongsTo(Client::class);
    }

}
