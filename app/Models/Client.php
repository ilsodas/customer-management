<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\City;

class Client extends Model
{
    use HasFactory;

    protected $table = 'client';

    protected $fillable = [
        'cod', 'name'
    ];

    public function city() {
        return $this->hasOne(City::class);
    }
}
