@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @if ($message = Session::get('success'))
                <div class="col-lg-12 alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif

            <div class="col"><h2>Create Clients</h2></div>
            <div class="w-100"></div>
        </div>

        <form method="POST" action="{{ route('client.store') }}">

            @method('POST')
            @csrf

            <table class="table table-bordered" style="width: 97%">
                <tr>
                    <td><label for="cod">COD: </label></td>
                    <td><input name="cod" value=""></td>
                </tr>
                <tr>
                    <td><label for="name">Client Name: </label></td>
                    <td><input name="name" value=""></td>
                </tr>
                <tr>
                    <td><label for="city">City Name: </label></td>
                    <td><input name="city" value=""></td>
                </tr>

                <tr>
                    <td colspan="2">
                        <div class="pull-right h3">
                            <button type="submit" class="btn btn-success">Save Client Record</button>
                        </div>
                    </td>
                </tr>
            </table>

        </form>

    </div>
@endsection
