@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            @if ($message = Session::get('success'))
                <div class="col-lg-12 alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif

            <div class="col"><h2>Manage Clients</h2></div>
            <div class="w-100"></div>
            <div class="col">
                <div class="pull-right h3">
                    <a href="{{ route('client.create') }}" class="btn btn-success">Create New Client Record</a>
                </div>
            </div>

            <table class="table table-bordered" style="width: 97%">
                <tr>
                    <th>No</th>
                    <th>COD</th>
                    <th>Name</th>
                    <th>City</th>
                    <th>Action</th>
                </tr>

                @foreach($client as $key => $data)

                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $data->cod }}</td>
                        <td>{{ $data->name }}</td>
                        <td>{{ $data->city->name }}</td>
                        <td>
                            <form action="{{ route('client.destroy', $data->id) }}" method="POST">
                                <a href="{{ route('client.edit', $data->id) }}" class="btn btn-primary">Edit</a>

                                @csrf
                                @method('DELETE')

                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </td>
                    </tr>

                @endforeach

                {{ $client->links() }}
            </table>

        </div>
    </div>
@endsection
