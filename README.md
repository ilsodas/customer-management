<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

## About this Laravel Project

How to get started, go to the root directory of this project and run the following
- `ddev config`
- `ddev start`
- `npm i` 
- `npm run dev`
- `php artisan migrate` 
- `php artisan db:seed` 

** Just a note if any or some of the command do not work just add ddev exec before the command `ddev exec php artisan migrate`

Lists of routes
- /client
- /client/{client}/edit
- /client/{client}
